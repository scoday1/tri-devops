FROM alpine:latest
MAINTAINER scoday
VOLUME tri-data
ONBUILD RUN mkdir /tri-data/
ONBUILD RUN mkdir /triz
COPY produce /
COPY serve / 
COPY haproxy.cfg /etc/haproxy/haproxy.cfg
WORKDIR /tri-data
RUN apk add --update \
    bash \
    python-dev \
    py-pip \
    haproxy \
    build-base 
RUN pip install mkdocs
EXPOSE 8000
